ARG PYTHON_VERSION
ARG PYTHON_IMAGE_TYPE

FROM python:$PYTHON_VERSION-$PYTHON_IMAGE_TYPE

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
# pip
ENV PIP_NO_CACHE_DIR=off
ENV PIP_DISABLE_PIP_VERSION_CHECK=on
ENV PIP_DEFAULT_TIMEOUT=100

# Установка пакетов для сборки бинарника psycopg и python-magic
RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc libc6-dev libpq-dev libmagic1

# Установка poetry
RUN python -m pip install --upgrade pip && \
    pip install poetry==1.2.2 && \
    poetry config virtualenvs.create false
