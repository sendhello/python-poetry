ARG PYTHON_VERSION
ARG PYTHON_IMAGE_TYPE

FROM python:$PYTHON_VERSION-$PYTHON_IMAGE_TYPE

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
# pip
ENV PIP_NO_CACHE_DIR=off
ENV PIP_DISABLE_PIP_VERSION_CHECK=on
ENV PIP_DEFAULT_TIMEOUT=100

RUN apk add --no-cache gcc libffi-dev musl-dev postgresql-dev

# Установка poetry
RUN python -m pip install --upgrade pip && \
    pip install poetry==1.2.2 && \
    poetry config virtualenvs.create false
